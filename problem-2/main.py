class Counter(object):
    def __init__(self, func):
        self.called = 0
        self.func = func
        self.table = {}

    def __call__(self, *args, **kwargs):
        self.called += 1

        kwtuple = tuple((key, kwargs[key]) for key in kwargs.keys())
        key2 = (args, kwtuple)

        try:
            return self.table[key2]
        except KeyError:
            value = self.func(*args, **kwargs)
            self.table[kwtuple] = value

            return value

    def __len__(self):
        return len(self.table)


@Counter
def square(x, **kwargs):
    return x * x

print(square(2, name='henrique', last_name='leal'))
print(square(2))
print(square(3))
